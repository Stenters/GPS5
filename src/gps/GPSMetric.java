package gps;


/**
* Functional interface representing a Metric given GPSTrack data
* @author wojciechowskia
 */
@FunctionalInterface
public interface GPSMetric {

	/**
	 * Calculates and formats a metric given GPSTrack data
	 * @param track the track to calculate a metric for
	 */
	String getMetric(GPSTrack track);

}