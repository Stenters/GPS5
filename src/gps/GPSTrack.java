/**
 * @author evensonje
 * @version 1.0
 * @created 25-Oct-2018 13:13:03
 */

package gps;

import java.util.List;

/**
 * models a GPS course based on GPS coordinates
 * @author evensonje
 *
 */
public class GPSTrack {

	private String name;
	private List<GPSPoint> points;
	
	/**
	 * Default constructor of the class
	 * @param points list of GPS points in the track
	 * @param name title of the track
	 */
	public GPSTrack(List<GPSPoint> points, String name) {
		this.name = name;
		this.points = points;
	}
	
	/**
	 * 
	 * @return the title of the track
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * 
	 * @return the list of points that comprise the track
	 */
	public List<GPSPoint> getPoints() {
		return points;
	}
}
