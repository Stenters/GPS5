package gps;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ComboBox;
import javafx.stage.FileChooser;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import static java.util.stream.Collectors.toList;


/**
 * Controller for handling user interaction with the GUI
 * @author enterss
 * @version 1.0
 * @created 25-Oct-2018 13:13:03
 */
public class GPSController {
	
	@FXML
	private ComboBox<String> comboBox;
	@FXML
	private Label nameLabel; 
	@FXML
	private Label latLabel;
	@FXML
	private Label lonLabel;
	@FXML
	private Label distLabel;
	@FXML
	private Label avgSpeedLabel;
	@FXML
	private Label maxSpeedLabel;
	@FXML
	private Label minElevationLabel;
	@FXML
	private Label maxElevationLabel;
	
	
	private List<GPSTrack> tracks;
	private boolean onePointWarningShown;

	@FXML
	/*
	  Method for handling the user selecting open
	 */
	private void openEventHandler(){
		clearMetrics();
	    try{
            // Query user for file to load
            List<String> files = getFiles();
            
            if (files == null || files.size() == 0) {
            	throw new NullPointerException();
            }

            if (files.size() > 10) {
            	removeFilesMoreThan10(files);
            }
            
            // Parse the files
            GPSParserResult result = GPSFileFacade.parseGPSData(files);
            tracks = result.getGPSTracks();

            // Check for invalid files in the input
            if (result.getInvalidFilesNames().size() > 0){
                showInvalidFileWarning(result.getInvalidTrackInformation());
            }

            // Add the names of the tracks to the combo box
            comboBox.getItems().addAll(result.getGPSTracks().stream().map(GPSTrack::getName).collect(toList()));

            // Resize dropdown to the size of elments
            comboBox.show();
            comboBox.autosize();
            
            // Select the first element in the combo box
            initialize();
            
        } catch (NullPointerException e){
	        // Cancel was called by File Chooser
            // do nothing
	    } catch (Exception e){
            Alert alert = new Alert(AlertType.WARNING);
            alert.setHeaderText("Warning: Illegal File Selected");
            alert.setContentText("There was an error while initializing the parser");
            alert.showAndWait();
        }
	}

    private void initialize() {
    	// Enable the combo box (if first time running)i
    	if (comboBox.isDisable()) {    		
    		comboBox.setDisable(false);
    	}
    	
        if (!comboBox.getSelectionModel().isSelected(0)){
            comboBox.getSelectionModel().select(0);
        }
        
        comboBox.setVisibleRowCount(10);
    }

    private void removeFilesMoreThan10(List<String> files) {
        List<String> removedFiles = new ArrayList<>();
        while (files.size() > 10){
            removedFiles.add(files.remove(files.size() - 1));
        }
        StringBuilder contentText = new StringBuilder(
                "Only 10 files may be loaded, the following files will not be parsed.");
        buildFileNameList(contentText, removedFiles);
        
        Alert alert = new Alert(AlertType.WARNING);
        alert.setHeaderText("Warning: Too Many Files Selected");
        alert.setContentText(contentText.toString());
        alert.showAndWait();
    }

    private void showInvalidFileWarning(Map<String, String> invalidFiles) {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setHeaderText("Warning: Illegal File Format");
        StringBuilder contentText = new StringBuilder(
                "The following files are not formatted properly\t\t\t\t\t\t\t");
       
        for (String fileName : invalidFiles.keySet()) {
            File file = new File(fileName);
        	contentText.append("\n\t").append(file.getName()).append(" because:\n\t\t").append(invalidFiles.get(fileName));
        }

        alert.getDialogPane().setMinWidth(300);
        alert.setWidth(300);


        alert.setContentText(contentText.toString());
        alert.showAndWait();
    }
    
    private void buildFileNameList(StringBuilder builder, List<String> filePaths) {
    	for(String path : filePaths) {
    		File file = new File(path);
    		builder.append("\n\t\t").append(file.getName());
    	}
    }
    
    private List<String> getFiles() {
        // Use a file chooser to select the desired file
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Select 1 - 10 GPX files");
        chooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        List<File> files = chooser.showOpenMultipleDialog(new Stage());

        // Return the list of files
        return files.stream().map(File::getAbsolutePath).collect(toList());
    }

    @FXML
    /*
      Method for handling the event where the user selects which file they are viewing
     */
	private void trackSwitchedEventHandler(){

    	// Get the selected track
    	String fileName = comboBox.getSelectionModel().getSelectedItem();
    	GPSTrack track = tracks.stream().filter(iterTrack -> iterTrack.getName().equals(fileName)).findFirst().orElse(null);
    	
    	if(track != null) {
        	// Load the metrics from the track
        	updateMetric(Metrics::getName, nameLabel, track);
        	updateMetric(Metrics::getMinAndMaxLatitude, latLabel, track);
        	updateMetric(Metrics::getMinAndMaxLongitude, lonLabel, track);
        	updateMetric(Metrics::getTotalDistance, distLabel, track);
        	updateMetric(Metrics::getAverageSpeed, avgSpeedLabel, track);
        	updateMetric(Metrics::getMaxSpeed, maxSpeedLabel, track);
        	updateMetric(Metrics::getMinimumElevation, minElevationLabel, track);
        	updateMetric(Metrics::getMaximumElevation, maxElevationLabel, track);
        	this.onePointWarningShown = false;
    	}
    }
    
    private void updateMetric(GPSMetric metric, Label metricLabel, GPSTrack track) {
    	String metricValue = metric.getMetric(track);
    	if(!this.onePointWarningShown && metricValue.equals(Metrics.NO_METRIC)) {
    		Alert alert = new Alert(AlertType.INFORMATION);
    		alert.setHeaderText("One Track Point");
    		alert.setContentText("This track only contains one point, so metrics involving speed and distance cannot be calculated. "
    							+ "These metrics will be displayed with a - .");
    		alert.showAndWait();
    		this.onePointWarningShown = true;
    	}
		metricLabel.setText(metricValue);
	}
    
    private void clearMetrics() {
		comboBox.getItems().clear();
		comboBox.setDisable(true);
		this.nameLabel.setText("");
		this.latLabel.setText("");
		this.lonLabel.setText("");
		this.distLabel.setText("");
		this.avgSpeedLabel.setText("");
		this.maxSpeedLabel.setText("");
		this.minElevationLabel.setText("");
		this.maxElevationLabel.setText("");
    }
    
}