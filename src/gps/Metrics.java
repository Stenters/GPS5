package gps;

import java.text.DecimalFormat;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.function.ToDoubleFunction;
import java.util.ArrayList;

/**
 * A class which provides all concrete implementations of GPSMetric
 * 
 * @author Andrew Wojciechowski
 */
public final class Metrics {
	private static final double EARTH_RADIUS = 6357000; 
	private static final double KM_TO_MILES_CONVERSION_FACTOR = 0.62;
	private static final double SECONDS_TO_HR = 3600;
	public static final String NO_METRIC = "-";
	//Limit metrics involving speed and distance calculations to 3 significant figures
	private static final DecimalFormat SPEED_AND_DISTANCE_FORMAT = new DecimalFormat("#.###");
	
	private Metrics(){
		//Prevent direct instantiation of Metrics
	}

	/**
	 * Calculates the average speed given a GPSTrack by calculating the total distance
	 * of the track and dividing it by the total time of the track. If a track only contains 1
	 * point a - is used to indicate that the speed cannot be calculated.
	 * 
	 * @param track the GPS track object
	 * @return the average speed in km/hr and mi/hr
	 */
	public static String getAverageSpeed(GPSTrack track){
		List<GPSPoint> points = track.getPoints();
		if(points.size() > 1) {
			double totalTime = secondsToHr(getTotalTime(track));
			double totalDistance = getNumericTotalDistance(track);
			
			String kmPerHourResult = SPEED_AND_DISTANCE_FORMAT.format(totalDistance / totalTime);
			String miPerHourResult = SPEED_AND_DISTANCE_FORMAT.format(kilometersToMiles(totalDistance) / totalTime);
			
			return String.format("%s km/hr - %s mi/hr", kmPerHourResult, miPerHourResult);
		}
		return NO_METRIC;
	}

	/**
	 * Calculates the minimum and maximum latitude over all points in a GPS track.
	 * The default decimal precision of String.format is used of 6 decimal places.
	 * 
	 * @param track the GPS track object
	 * @return the maximum latitude formatted in units of degrees
	 */
	public static String getMinAndMaxLatitude(GPSTrack track){
		double minLatitude = getMinValue(track, GPSPoint::getLatitude);
		double maxLatitude = getMaxValue(track, GPSPoint::getLatitude);
		return String.format("%f\u00b0 - %f\u00b0", minLatitude, maxLatitude);
	}

	/**
	 * Calculates the minimum and maximum longitude over all points in a GPS track.
	 * The default decimal precision of String.format is used of 6 decimal places.
	 * 
	 * @param track the GPS track object
	 * @return the minimum and maximum longitude formatted in units of degrees
	 */
	public static String getMinAndMaxLongitude(GPSTrack track){
		double minLongitude = getMinValue(track, GPSPoint::getLongitude);
		double maxLongitude = getMaxValue(track, GPSPoint::getLongitude);
		return String.format("%f\u00b0 - %f\u00b0", minLongitude, maxLongitude);
	}
	
	/**
	 * Calculates the minimum elevation over all points in a GPS track formatted to 3 significant figures
	 * 
	 * @param track the GPS track object
	 * @return the minimum elevation in km and mi
	 */
	public static String getMinimumElevation(GPSTrack track) {
		double minElevation = metersToKilometers(getMinValue(track, GPSPoint::getElevation));
		double miElevation = kilometersToMiles(minElevation);
		String kmResult = SPEED_AND_DISTANCE_FORMAT.format(minElevation);
		String miResult = SPEED_AND_DISTANCE_FORMAT.format(miElevation);
		return String.format("%s km - %s miles", kmResult, miResult);
	}
	
	/**
	 * Calculates the maximum elevation over all points in a GPS track formatted to 3 significant figures
	 * 
	 * @param track the GPS track object
	 * @return the minimum elevation in km and mi
	 */
	public static String getMaximumElevation(GPSTrack track) {
		double maxElevation = metersToKilometers(getMaxValue(track, GPSPoint::getElevation));
		double miElevation = kilometersToMiles(maxElevation);
		String kmResult = SPEED_AND_DISTANCE_FORMAT.format(maxElevation);
		String miResult = SPEED_AND_DISTANCE_FORMAT.format(miElevation);
		return String.format("%s km - %s miles", kmResult, miResult);
	}
	
	/**
	 * Calculates the maximum speed between two points in a GPS track.
	 * 
	 * @param track the GPS track object
	 * @return the maximum speed in km/hr and mi/hr
	 */
	public static String getMaxSpeed(GPSTrack track){
		List<GPSPoint> points = track.getPoints();
		if(points.size() > 1) {
			List<Double> speeds = getSpeedsBetweenPoints(track);
			double numericMaxSpeed = speeds.stream().mapToDouble(x -> x).max().getAsDouble();
			String maxSpeed = SPEED_AND_DISTANCE_FORMAT.format(numericMaxSpeed);
			String miPerHourResult = SPEED_AND_DISTANCE_FORMAT.format(kilometersToMiles(numericMaxSpeed));
			
			return String.format("%s km/hr - %s mi/hr", maxSpeed, miPerHourResult);
		}
		return NO_METRIC;
	}

	/**
	 * Gets the name of the GPS track
	 * 
	 * @param track the GPS track object
	 */
	public static String getName(GPSTrack track){
		return track.getName();
	}

	/**
	 * Gets the total distance traveled over the entire track.
	 * If a track only contains 1 point a - is returned indicating
	 * that the total distance of the track cannot be calculated
	 * 
	 * @param track the gps track object
	 * @return the total distance in km and miles
	 */
	public static String getTotalDistance(GPSTrack track){
		List<GPSPoint> points = track.getPoints();
		if(points.size() > 1) {
			double numericTotalDistance = getNumericTotalDistance(track);
			
			String kmResult = SPEED_AND_DISTANCE_FORMAT.format(numericTotalDistance);
			String miles = SPEED_AND_DISTANCE_FORMAT.format(kilometersToMiles(numericTotalDistance));
			
			return String.format("%s km - %s miles", kmResult, miles);
		}
		return NO_METRIC;
	}
	
	private static double getNumericTotalDistance(GPSTrack track) {
		List<GPSPoint> points = track.getPoints();
		double totalDistanceBetweenPoints = 0;
		for(int i = 1; i < points.size(); i++) {
			totalDistanceBetweenPoints += getDistanceBetweenPoints(points.get(i - 1), points.get(i));
		}
		return metersToKilometers(totalDistanceBetweenPoints);
	}
	
	private static double getTotalTime(GPSTrack track) {
		List<GPSPoint> points = track.getPoints();
		double totalTime = 0;
		for(int i = 1; i < points.size(); i++) {
			totalTime += getTimeBetweenPoints(points.get(i - 1), points.get(i));
		}
		return totalTime;
	}
	
	private static List<Double> getSpeedsBetweenPoints(GPSTrack track) {
		List<Double> result = new ArrayList<Double>();
		List<GPSPoint> points = track.getPoints();
		for(int i = 1; i < points.size(); i++) {
			GPSPoint point1 = points.get(i - 1);
			GPSPoint point2 = points.get(i);
			
			double distance = metersToKilometers(getDistanceBetweenPoints(point1, point2));
			result.add(distance / secondsToHr(getTimeBetweenPoints(point1, point2)));
		}
		return result;
	}
	
	private static double getTimeBetweenPoints(GPSPoint point1, GPSPoint point2) {
		return ChronoUnit.SECONDS.between(point1.getTime(), point2.getTime());
	}
	
	private static double getDistanceBetweenPoints(GPSPoint point1, GPSPoint point2) {
		double deltaX = (EARTH_RADIUS + (point2.getElevation() + point1.getElevation()) / 2) * 
						(Math.toRadians(point2.getLongitude()) - Math.toRadians(point1.getLongitude())) 
						* (Math.cos(Math.toRadians((point2.getLatitude() + point1.getLatitude()) / 2))); 
		double deltaY = (EARTH_RADIUS + (point2.getElevation() + point1.getElevation()) / 2) *
						(Math.toRadians(point2.getLatitude()) - Math.toRadians(point1.getLatitude()));
		double deltaZ = point2.getElevation() - point1.getElevation();
		
		return Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2) + Math.pow(deltaZ, 2));
	}
	
	private static double metersToKilometers(double meters) {
		return meters * Math.pow(10, -3);
	}
	
	private static double kilometersToMiles(double kilometers) {
		return kilometers * KM_TO_MILES_CONVERSION_FACTOR;
	}
	
	private static double getMinValue(GPSTrack track, ToDoubleFunction<? super GPSPoint> function) {
		return track.getPoints().stream().mapToDouble(function).min().getAsDouble();
	}
	
	private static double getMaxValue(GPSTrack track, ToDoubleFunction<? super GPSPoint> function) {
		return track.getPoints().stream().mapToDouble(function).max().getAsDouble();
	}
	
	private static double secondsToHr(double seconds) {
		return seconds * (1 / SECONDS_TO_HR);
	}
}