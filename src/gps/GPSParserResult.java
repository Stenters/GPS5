package gps;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author Shreyesh Patel
 * @version 1.0
 * @created 30-Oct-2018 11:53
 */
public class GPSParserResult {
	private List<String> invalidFileNames;
	private List<GPSTrack> gpsTracks;
	private Map<String, String> invalidTrackInformation;
	
	/**
	 * Constructor for GPSParserResult
	 * @param invalidFileNames
	 * @param gpsTracks
	 * @param invalidTrackInformation
	 */
	public GPSParserResult(List<String> invalidFileNames, List<GPSTrack> gpsTracks, Map<String, String> invalidTrackInformation) {
		this.invalidFileNames = invalidFileNames;
		this.gpsTracks = gpsTracks;
		this.invalidTrackInformation = invalidTrackInformation;
	}
	
	/**
	 * Creates an unmodifiable list of invalid file names
	 * @return List of invalid file names
	 */
	public List<String> getInvalidFilesNames(){
		return Collections.unmodifiableList(invalidFileNames);
	}
	
	/**
	 * Creates an unmodifiable list of GPS tracks
	 * @return List of GPS tracks
	 */
	public List<GPSTrack> getGPSTracks(){
		return Collections.unmodifiableList(gpsTracks);
	}
	
	/**
	 * Creates an unmodifiable map of the invalid track information
	 * @return List of GPS tracks
	 */
	public Map<String, String> getInvalidTrackInformation(){
		return Collections.unmodifiableMap(invalidTrackInformation);
	}
}