package gps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.xml.sax.SAXException;
import se2030.parser.Parser;

/**
 * Facade class to parse a list of files in order to create tracks.
 * @author Shreyesh Patel
 * @version 1.0
 * @created 25-Oct-2018 13:13:03
 */
public final class GPSFileFacade {

	private GPSFileFacade() {
		//Prevent direct instantiation of GPSFileFacade
	}

	/**
	 * Parses through the list of file names, to create a list of tracks
	 * and invalid file names. If a SAXException is caught, that file name will
	 * be added to the list of invalid names. 
	 * @param fileNames
	 * @return GPSParserResult
	 * @throws Exception
	 */
	public static GPSParserResult parseGPSData(List<String> fileNames) throws Exception{
		List<GPSTrack> listOfTracks = new ArrayList<>();
		List<String> invalidFileNames = new ArrayList<>();
		Map<String, String> invalidTrackInformation = new HashMap<>();
		GPSParserEventHandler parserEventHandler = new GPSParserEventHandler();

		for(String fileName : fileNames) {
			try {
				listOfTracks.add(parseTrack(fileName, parserEventHandler));
			} catch(SAXException e){
				invalidTrackInformation.put(fileName, e.getMessage() 
						+ " Column: " +parserEventHandler.getColumn() + " Line: " + parserEventHandler.getLine());
				invalidFileNames.add(fileName);
			}
		}

		return new GPSParserResult(invalidFileNames, listOfTracks, invalidTrackInformation);
	}
	
	private static GPSTrack parseTrack(String fileName, GPSParserEventHandler eventHandler) throws Exception {
		eventHandler.enableLogging(false);
		Parser parser = new Parser(eventHandler);
		parser.parse(fileName);
		return eventHandler.getGPSTrack();
	}

}