package gps;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * @author enterss
 * @version 1.0
 * @created 25-Oct-2018 13:13:02
 */
public class GPSApplication extends Application {
	private static final int PREF_WIDTH = 600;
	private static final int PREF_HEIGHT = 400;

	/**
    * The start of the application
    * @param primaryStage the stage to display to
    * @throws Exception if everything goes wrong
    */
    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("..\\GPS.fxml"));
        primaryStage.setTitle("GPS Reader");
        primaryStage.setScene(new Scene(root, PREF_WIDTH, PREF_HEIGHT));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }

}