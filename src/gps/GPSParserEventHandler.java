package gps;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import se2030.parser.AbstractParserEventHandler;

/**
 * A concrete implementation of a SAX parser event handler
 * used for parsing GPX files
 * @author Andrew Wojciechowski
 */
public class GPSParserEventHandler extends AbstractParserEventHandler {
	//The final state does not need to be handled since we validate that the gpx
	//element must be the first element in the gpx document and the only ways
	//to generate invalid xml must be if the gpx document does not end with a 
	//</gpx>, if the document contains more than one root element, or if the
	//file ends prematurely which all of the scenarios are handled by internal SAX code
	private enum GPXParserState {INITIAL, GPX, TRK, NAME, TRKSEG, TRKPT, TIME, ELE};
	private GPXParserState state;
	private String trackName;
	private Double currentLattitude;
	private Double currentLongitude;
	private Double currentElevation;
	private LocalDateTime currentTime;
	private List<GPSPoint> points;
	
	private static final double INVALID_LATITUDE_VALUE = -90;
	private static final double INVALID_LONGITUDE_VALUE = -180;
	private static final DateTimeFormatter TIME_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss[.SSS]'Z'");
	
	/**
	 * Initializes the initial state and the current point values for the handler
	 */
	public GPSParserEventHandler() {
		this.state = GPXParserState.INITIAL;
		this.resetPointValues();
		this.points = new ArrayList<GPSPoint>();
	}

	/**
	 * Handles the current state of the gpx parsing when a new element starts. All custom
	 * document formatting handling is done in this method. 
	 * 
	 * @param uri unused
	 * @param localName the local name of the element
	 * @param qName unused
	 * @param attrs the element attributes
	 */
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attrs) 
			throws SAXException {
		switch(this.state) {
			case INITIAL:
				advanceState(localName, "gpx", GPXParserState.GPX);
				break;
			case GPX:
				advanceState(localName, "trk", GPXParserState.TRK);
				break;
			case TRK:
				advanceState(localName, "name", GPXParserState.NAME);
				break;
			case NAME:
				advanceState(localName, "trkseg", GPXParserState.TRKSEG);
				break;
			case TRKSEG:
				advanceState(localName, "trkpt", GPXParserState.TRKPT);
				parseLatitudeAndLongitudeAttributes(attrs);
				break;
			case TRKPT:
				advancePointState(localName);
				break;
			case ELE:
				throw new SAXException("ele elements cannot contain child elements");
			case TIME:
				throw new SAXException("time elements cannot contain child elements");
		}
	}
	/**
	 * Handles the state for when an element ends in a GPX document. No custom error handling
	 * is needed for this method
	 * 
	 * @param uri unused
	 * @param localName the local name of the element
	 * @param qName unused
	 */
	@Override
	public void endElement(String uri, String localName, String qName) 
			throws SAXException {
		if(localName.equalsIgnoreCase("trkpt")) {
			validateMissingPointElements();
			this.state = GPXParserState.TRKSEG;
			this.points.add(new GPSPoint(this.currentLattitude, this.currentLongitude, this.currentElevation, this.currentTime));
			resetPointValues();
		} else if(localName.equalsIgnoreCase("ele") || localName.equalsIgnoreCase("time")) {
			this.state = GPXParserState.TRKPT;
		}
	}
	
	/**
	 * Handles the situation when text is found in a GPX element
	 * 
	 * @param ch an array of characters representing the xml document text
	 * @param start the starting position of the characters
	 * @param the length of the text
	 */
	@Override
	public void characters(char[] ch, int start, int length) 
			throws SAXException {
		switch(this.state) {
			case NAME:
				this.trackName = parseValue(ch, start, length, this.trackName);
				break;
			case ELE:
				this.currentElevation = Double.parseDouble(parseValue(ch, start, length, this.currentElevation));
				break;
			case TIME:
				String time = parseValue(ch, start, length, this.currentTime);
				validateCurrentTime(time);
				break;
			default:
				break;
		}
	}
	
	/**
	 * Getter for the parsed GPS track
	 * 
	 * @return the parsed GPS track
	 */
	public GPSTrack getGPSTrack(){
		return new GPSTrack(this.points, this.trackName);
	}
	
	private void advanceState(String elementLocalName, String expectedName, GPXParserState nextState) throws SAXException {
		if(!elementLocalName.equalsIgnoreCase(expectedName)) {
			throw new SAXException(String.format("%s element found in the wrong location", expectedName));
		}
		this.state = nextState;
	}
	
	private String parseCharacters(char[] characters, int start, int length) {
		String s = new String(characters);
		s = s.substring(start, start+length); 
		return s.trim(); 
	}
	
	private void parseLatitudeAndLongitudeAttributes(Attributes attrs) throws SAXException {
		for(int i = 0; i < attrs.getLength(); i++) {
			if(attrs.getLocalName(i).equalsIgnoreCase("lat")) {
				this.currentLattitude = Double.parseDouble(attrs.getValue(i));
			} else if(attrs.getLocalName(i).equalsIgnoreCase("lon")) {
				this.currentLongitude = Double.parseDouble(attrs.getValue(i));
			}
		}
		validateAttribute(this.currentLattitude, INVALID_LATITUDE_VALUE, "latitude");
		validateAttribute(this.currentLongitude, INVALID_LONGITUDE_VALUE, "longitude");
	}
	
	private void validateAttribute(Double attributeValue, double minValue, String attributeName) throws SAXException {
		if(attributeValue == null) {
			throw new SAXException(String.format("%s attribute not found on trkpoint", attributeName));
		} else if(attributeValue < minValue || attributeValue > Math.abs(minValue)) {
			throw new SAXException(String.format("invalid %s value", attributeName));
		}
	}
	
	private void advancePointState(String localName) throws SAXException {
		if(localName.equalsIgnoreCase("ele")) {
			this.state = GPXParserState.ELE;
		} else if(localName.equalsIgnoreCase("time")) {
			this.state = GPXParserState.TIME;
		} else {
			throw new SAXException("invalid element found in trkpt");
		}
	}
	
	private void validateMissingPointElements() throws SAXException {
		if(this.currentElevation == null) {
			throw new SAXException("elevation missing in trkpt element");
		} else if(this.currentTime == null) {
			throw new SAXException("time missing in trkpt element");
		}
	}
	
	/**
	 * Handle SAX firing the characters event twice due to new line characters between elements
	 * This method will check to make sure that we only parse a value if we 
	 * don't have a value for a track name or point attribute already parsed
	 * 
	 * @param characters the xml characters
	 * @param start the starting index of the characters
	 * @param length the length of the xml element value
	 * @param currentValue the current track name or point attribute
	 * @return a string representation of the xml element value
	 */
	private String parseValue(char[] characters, int start, int length, Object currentValue) {
		if(currentValue == null) {
			return parseCharacters(characters, start, length);
		}
		return currentValue.toString();
	}
	
	private void validateCurrentTime(String time) throws SAXException {
		if(time != null) {
			try {
				this.currentTime = LocalDateTime.parse(time, TIME_FORMAT);
			} catch(DateTimeParseException e) {
				throw new SAXException("invalid time for trkpt");
			}
		}
	}
	
	private void resetPointValues() {
		this.currentLattitude = null;
		this.currentLongitude = null;
		this.currentElevation = null;
		this.currentTime = null;
	}
}