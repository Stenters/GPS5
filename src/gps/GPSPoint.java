package gps;

import java.time.LocalDateTime;

/**
 * Class for modeling GPS navigation points
 * @author evensonje
 * @version 1.0
 * @created 25-Oct-2018 13:13:03
 */
public class GPSPoint {

	private double lat;
	private double lon;
	private double elevation;
	private LocalDateTime time;
	
	/**
	 * Creates a GPS point with the given attributes
	 * @param lat latitude
	 * @param lon longitude
	 * @param elevation elevation above sea level
	 * @param time LocalDateTime formatted
	 */
	public GPSPoint(double lat, double lon, double elevation, LocalDateTime time){
		this.lat = lat;
		this.lon = lon;
		this.elevation = elevation;
		this.time = time;
	}

	/**
	 * 
	 * @return elevation of the point
	 */
	public double getElevation(){
		return elevation;
	}

	/**
	 * 
	 * @return the latitude, in degrees, of the point
	 */
	public double getLatitude(){
		return lat;
	}

	/**
	 * @return the longitude, in degrees, of the point
	 */
	public double getLongitude(){
		return lon;
	}
	
	/*
	 * @return the time the point was logged
	 */
	public LocalDateTime getTime() {
		return time;
	}
}