package se2030.parserdemo;

import se2030.parser.AbstractParserEventHandler;
import se2030.parser.Parser;

import java.util.List;

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/*
 * This app demonstrates how to use the Parser and ParserEventHandler to parse an xml file.
 */
public class ParserDemoApp /*implements IParserCompleteListener*/ {

	public static void main(String[] args) {

//		String filename = "GPSTest5Center.txt"; // gpx file to be parsed (in this case assumed to be in cwd)
//		String filename = "StudentList1.txt"; 
		String filename = "InvalidGPX.txt"; 

		AbstractParserEventHandler handler = new ParserEventHandler();
		handler.enableLogging(true); // enable debug logging to System.out

		Parser parser = null;
		try {
			parser = new Parser( handler ); // create the Parser			
			parser.parse( filename );		// parse a file!
		} catch (SAXException e) {
			// something went wrong during parsing; print to the console since this is a console demo app.
			System.out.println("ParserDemoApp: Parser threw SAXException: " + e.getMessage() );
			System.out.println("The error occurred near line " + handler.getLine() + ", col "+ handler.getColumn());
		} catch (Exception e) {
			// something went wrong when initializing the Parser; print to the console since this is a console demo app.
			System.out.println("ParserDemoApp: Parser threw Exception: " + e.getMessage() );
		}
	} 


}
