package se2030.parserdemo;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import se2030.parser.AbstractParserEventHandler;

/*
 * This class provides a sample concrete implementation of the AbstractParserEventHandler,
 * which is an abstract class that provides default implementations of the
 * event handling methods that are called by the Parser.
 *  
 *  You must override the methods below so that you can capture the xml 
 *  data of interest to your application.
 *  
 *  You must pass the reference to this class to the Parser constructor. 
 */
public class ParserEventHandler extends AbstractParserEventHandler {
	// possible state values
	private enum PossibleStates {INITIAL, GPX, TRK, NAME, TRKPT, TIME, ELE, FINAL}; // more states needed?
	private PossibleStates currentState = PossibleStates.INITIAL; // starting state of parsing
	// You need to declare some kind of coordinate collection class here.

	@Override
	// This ContentHandler method is called whenever the Parser encounters a new element tag
	// which may or may not have Attributes. For example,
	// <trkpt lat="43.30627219" lon="-87.98892299">
	// The <trkpt> element contains two attributes: lat and lon.
	// Conversely, <ele>249.5</ele> contains no attributes.
	// The code below shows how to access the values of the attributes when they exist.
	public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
		super.startElement(uri, localName, qName, atts );// super.startElement just logs a message to System.out

		// localName contains the name of the element - e.g. gpx, name, trkpt, etc

		// You need to ensure that elements are found in the correct order by maintaining state.
		// For instance:
		if( localName.equalsIgnoreCase("gpx") ) {
			if( currentState != PossibleStates.INITIAL ) { // <gpx> should be the first element found
				throw new SAXException("<gpx> element found in illegal location!");
				// You must make sure that no other element besides <gpx> is at the start of the xml file;
				// if some other element is found, you have an invalid file, so throw a SAXException.
			}
			currentState = PossibleStates.GPX; // once <gpx> is found, we're in the GPX state!
			// You need to initialize your coordinate collection class here.
		} 
	
		//TODO: add other logic to check for <trk> and advance to the TRK state

		if( localName.equalsIgnoreCase("trkpt")) {
			if( currentState != PossibleStates.TRK ) { // <trkpt> should only be found within <trk>
				throw new SAXException("<trkpt> element found in illegal location!");
			}
			// You must advance the state to TRKPT here.
			// You must create/initialize a new coordinate object whenever a <trkpt> is found.

			// atts is an object that contains the attribute name/value pairs, if attributes exist
			if( atts.getLength()> 0 ) { // check for element attributes
				for( int index=0; index<atts.getLength(); index++) {
					String attName = atts.getLocalName(index);
					String attValue = atts.getValue(index);
					System.out.println(attName + ":" + attValue );
					// You must check to make sure that the <trkpt> element has both lat and lon
					// attributes, so that you can set those values in the coordinate object.
					// If either of those attributes don't exist, throw a SAXException.
				}
			}
		}
		// You must check for other elements and make sure the state is correct as each is found;
		// for instance, an <ele> element can only be found within a <trkpt> element (in the TRKPT state),
		// a <name> element can only be found within a <trk> element (in the TRK state),
		// a <time> element can only be found within a <trkpt> element (in the TRKPT state), etc.
		// If not, throw a SAXException!
	}

	@Override
	// This ContentHandler method is called whenever the Parser encounters an end tag (e.g. </trkpt>, </gpx>)
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		// You'll override this method in your application to finalize your data
		super.endElement(uri, localName, qName); // super.endElement just logs a message to System.out

		// localName contains the name of the element - e.g. gpx, name, trkpt, etc
		if( localName.equalsIgnoreCase("gpx") ) {
			if( currentState != PossibleStates.GPX ) { // should be back in GPX state when </gpx> is encountered
				throw new SAXException("</gpx> element found in illegal location!");
			}
			currentState = PossibleStates.FINAL; // </gpx> signals that we've reached the end state!
			// When the FINAL state is reached, your collection of coordinates should be complete.
			// You will have to provide a public getter method to allow that collection to be retrieved.
		}
		// You must check for other end elements and make sure the state is correct as each is found;
		// for instance, an </trkpt> element can only be found if the state is TRKPT,
		// a </ele> element can only be found if the state is ELE, etc.
		// If not, throw a SAXException!

		// You must finalize the coordinate object whenever a </trkpt> is found, making
		// sure that you have fully initialized its lat, lon, time, and elevation attributes.
		// If it's not fully initialized, something was missing, so throw a SAXException in that case.

	}

	@Override
	// This ContentHandler method is called whenever the Parser encounters text between tags.
	// For example, for the tag <ele>249.5</ele>, the characters "249.5" are reported.
    // For the tag <time>2010-10-19T13:00:00Z</time>, the characters "2010-10-19T13:00:00Z" are reported.
	// For tags like <gpx> and <trkpt>, a newline character is reported.
	public void characters(char[] ch, int start, int length) throws SAXException {
		// You'll override this method to extract values for the elements that contain character data.
		super.characters(ch, start, length);// super.characters() just logs a message to System.out
		
		// convert the char[] array to a String for convenience
		String s = new String(ch);
		s = s.substring(start, start+length); 
		s = s.trim(); // remove leading and trailing whitespace
		
		//Note that elements like <ele> and <time> contain characters that must be converted into
		//  elevation and date/time values. If conversion fails, throw a SAXException!
	}
	
	@Override
	//This ContentHandler method is called when the Parser reaches the end of the
	// XML document. If the document was a well-formed GPS file, then the currentState of the
	// system should be possibleStates.FINAL (since the end </gpx> element was
	// found). If the currentState is not FINAL here, then something was wrong
	// with the document structure, so throw a SAXException!
	public void endDocument() throws SAXException {
		if( currentState != PossibleStates.FINAL )
			throw new SAXException("Document structure error. Not in FINAL state at the end of the document!");
		super.endDocument();// super.endDocument() just logs a message to System.out
	}

}