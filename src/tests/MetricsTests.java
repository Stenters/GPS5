package tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;
import java.util.Arrays;
import org.junit.BeforeClass;
import org.junit.Test;
import gps.GPSPoint;
import gps.GPSTrack;
import gps.Metrics;

public class MetricsTests {
	private static final double DELTA =1.0E-2;
	private static final double DISTANCE_DELTA =2.0E-1;
	
	private static GPSTrack testTrack;
	private static GPSTrack speedTestTrack;
	private static GPSTrack singlePointTestTrack;
	
	/**
	 * Creates the tracks before the test methods run.
	 */
	@BeforeClass
	public static void setup() {
		LocalDateTime date = LocalDateTime.of(2016, 2, 10, 13, 0, 0);
		GPSPoint point1 = new GPSPoint(43.30000000000000, -87.90000000000000, 500.0000000000000, date);
		GPSPoint point2 = new GPSPoint(43.30000000000000, -88.00000000000000, 2500.0000000000000, date);
		testTrack = new GPSTrack(Arrays.asList(point1, point2), "GPSTest-1");
		
		//Creates a Track with approximately 6.87 miles / 11.05619 kilometers traveled in 10 hours
		// Approx 68.70 miles / 111.19 km traveled
		GPSPoint point1B = new GPSPoint(43.00000000000000, -88.00000000000000, 500.0000000000000, date.minusHours(10));
		GPSPoint point2B = new GPSPoint(44.00000000000000, -88.00000000000000, 500.0000000000000, date);
		speedTestTrack = new GPSTrack(Arrays.asList(point1B, point2B), "GPSTest-2");
		
		//Creates a Track with a single point
		GPSPoint point1C = new GPSPoint(43.00000000000000, -88.00000000000000, 500.0000000000000, date);
		singlePointTestTrack = new GPSTrack(Arrays.asList(point1B), "GPSTest-3");
	}
	
	/**
	 * This will test the minimum and maximum latitude methods in the Metrics class
	 * and compare the results to the actual min and max latitude.
	 * @throws Exception
	 */
	@Test
	public void testMinAndMaxLatitude() throws Exception {
		// Expected
		String minLat = formatExpectedDegrees("43.300000");
		String maxLat = formatExpectedDegrees("43.300000");
		String expectedLatitude = minLat + " - " + maxLat;

		// Actual
		String actualLatitude = Metrics.getMinAndMaxLatitude(testTrack);
		
		assertEquals(expectedLatitude, actualLatitude);
	}
	
	/**
	 * This will test the minimum and maximum longitude methods in the Metrics class
	 * and compare the results to the actual min and max longitude.
	 * @throws Exception
	 */
	@Test
	public void testMinAndMaxLongitude() throws Exception {
		// Expected
		String minLong = formatExpectedDegrees("-88.000000");
		String maxLong = formatExpectedDegrees("-87.900000");
		String expectedLongitude = minLong + " - " + maxLong;

		// Actual
		String actualLongitude = Metrics.getMinAndMaxLongitude(testTrack);
		
		assertEquals(expectedLongitude, actualLongitude);
	}
	
	/**
	 * Tests the minimum elevation in the Metrics class
	 */
	@Test
	public void testMinElevation() {
		//Act
		String result = Metrics.getMinimumElevation(testTrack);
		
		//Assert
		assertEquals("0.5 km - 0.31 miles", result);
	}
	
	/**
	 * Tests the maximum elevation in the Metrics class
	 */
	@Test
	public void testMaxElevation() {
		//Act
		String result = Metrics.getMaximumElevation(testTrack);
		
		//Assert
		assertEquals("2.5 km - 1.55 miles", result);
	}
	
	/**
	 * This will test the get name method in the Metrics class
	 * and compare the results to the actual name of the file.
	 * @throws Exception
	 */
	@Test
	public void testGetName() throws Exception{
		// Expected
		String expectedName = "GPSTest-1";
		// Actual
		String actualName = Metrics.getName(testTrack);
		
		assertEquals(expectedName, actualName);
	}
	
	
	
	@Test
	/**
	 * Tests average speed of a track in kilometers per hour
	 */
	public void testAverageSpeedKPH(){
		// Expected - 
		double expectedKPH = 11.096;
		
		// Actual
		double actualExpectedSpeed= parseSpeedKPH(Metrics.getAverageSpeed(speedTestTrack));
		
		assertEquals(expectedKPH, actualExpectedSpeed, DELTA);
	}
	
	@Test
	/**
	 * Tests average speed of a track in miles per hour
	 */
	public void testAverageSpeedMPH(){
		// Expected - 
		double expectedMPH = 6.879;
		
		// Actual
		double actualExpectedSpeed= parseSpeedMPH(Metrics.getAverageSpeed(speedTestTrack));
		
		assertEquals(expectedMPH, actualExpectedSpeed, DELTA);
	}
	
	@Test
	/**
	 * Tests average speed of  a track with a single point
	 */
	public void testAverageSpeedSinglePointTrack(){
	
		String expectedAverageSpeed = "-";
		// Actual
		String actualExpectedSpeed= Metrics.getAverageSpeed(singlePointTestTrack);
		
		assertEquals(expectedAverageSpeed, actualExpectedSpeed);
	}
	
	@Test
	/**
	 * Tests total distance of a track in kilometers
	 */
	public void testTotalDistanceKilos(){
		// Expected - 
		double expectedKilos = 111.00;
		
		// Actual
		double actualExpectedSpeed= parseDistanceKilos(Metrics.getTotalDistance(speedTestTrack));
		
		assertEquals(expectedKilos, actualExpectedSpeed, DISTANCE_DELTA);
	}
	
	@Test
	/**
	 * Tests total distance of a track in miles
	 */
	public void testTotalDistanceMiles(){
		// Expected - 
		double expectedMiles = 68.79;
		
		// Actual
		double actualExpectedSpeed= parseDistanceMiles(Metrics.getTotalDistance(speedTestTrack));
		
		assertEquals(expectedMiles, actualExpectedSpeed, DISTANCE_DELTA);
	}
	
	@Test
	/**
	 * Tests total distance of  a track with a single point
	 */
	public void testTotalDistanceSinglePointTrack(){
	
		String expectedAverageSpeed = "-";
		// Actual
		String actualExpectedSpeed= Metrics.getTotalDistance(singlePointTestTrack);
		
		assertEquals(expectedAverageSpeed, actualExpectedSpeed);
	}
	
	private String formatExpectedDegrees(String expected) {
		return String.format("%s\u00b0", expected);
	}
	
	private double parseSpeedKPH(String speedMetric) {
		double kiloPerHour = Double.parseDouble(speedMetric.substring(0,5));
		return kiloPerHour;
	}
	
	private double parseSpeedMPH(String speedMetric) {
		double milesPerHour = Double.parseDouble(speedMetric.substring(14,19));
		return milesPerHour;
	}
	
	private double parseDistanceKilos(String speedMetric) {
		double kilos = Double.parseDouble(speedMetric.substring(0,5));
		return kilos;
	}
	
	private double parseDistanceMiles(String speedMetric) {
		double miles = Double.parseDouble(speedMetric.substring(12,17));
		return miles;
	}
}
