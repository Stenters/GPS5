package tests;

import static org.junit.Assert.assertEquals;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.xml.sax.SAXException;

import gps.GPSParserEventHandler;
import gps.GPSPoint;
import se2030.parser.Parser;

import java.util.List;

/**
 * Integration tests for the GPSParserEventHandler tests
 * 
 * @author wojciechowskia
 *
 */
public class GPSParserEventHandlerTests {
    private static final double DELTA = 1.0E-10;
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    private static final String filePath = "./src/testFiles/";
    
    /**
     * Tests the case when a gpx element is not the root of a XML file
     * 
     * @throws Exception if there was an issue initializing the Parser
     */
	@Test
	public void test_notAGPXFile() throws Exception {
		//Arrange
		expectException(SAXException.class, "gpx element found in the wrong location");
		GPSParserEventHandler handler = createHandler();
		Parser parser = new Parser(handler);

		//Act
		parser.parse(filePath + "InvalidGPX.txt");
	}
	
	/**
	 * Tests the case when the track element is missing from a GPX file
	 * 
	 * @throws Exception if there was an issue initializing the Parser
	 */
	@Test
	public void test_noTrackElement() throws Exception {
		//Arrange
		expectException(SAXException.class, "trk element found in the wrong location");
		GPSParserEventHandler handler = createHandler();
		Parser parser = new Parser(handler);

		//Act
		parser.parse(filePath + "InvalidGPX2.txt");
	}
	
	/**
	 * Tests the case when a track segment element is missing from a GPX file
	 * 
	 * @throws Exception if there was an issue initializing the Parser
	 */
	@Test
	public void test_noTrackSegment() throws Exception {
		//Arrange
		expectException(SAXException.class, "trkseg element found in the wrong location");
		GPSParserEventHandler handler = createHandler();
		Parser parser = new Parser(handler);

		//Act
		parser.parse(filePath + "InvalidGPX3.txt");
	}
	
	/**
	 * Test the case when latitude is missing from a track point
	 * 
	 * @throws Exception if there was an issue initializing the Parser
	 */
	@Test
	public void test_MissingLatitude() throws Exception {
		//Arrange
		expectException(SAXException.class, "latitude attribute not found on trkpoint");
		GPSParserEventHandler handler = createHandler();
		Parser parser = new Parser(handler);
		
		//Act
		parser.parse(filePath + "gpstest-missing latiitude.txt");
	}
	
	/**
	 * Tests the case when longitude is missing from a track point
	 * 
	 * @throws Exception if there was an issue initializing the Parser
	 */
	@Test
	public void test_MissingLongitude() throws Exception {
		//Arrange
		expectException(SAXException.class, "longitude attribute not found on trkpoint");
		GPSParserEventHandler handler = createHandler();
		Parser parser = new Parser(handler);

		//Act
		parser.parse(filePath + "gpstest-missing longitude.txt");
	}
	
	/**
	 * Test the case when the latitude of the point is outside the range for a valid latitude
	 * 
	 * @throws Exception if there was an issue initializing the Parser
	 */
	@Test
	public void test_BadLatitude() throws Exception {
		//Arrange
		expectException(SAXException.class, "invalid latitude value");
		GPSParserEventHandler handler = createHandler();
		Parser parser = new Parser(handler);

		//Act
		parser.parse(filePath + "gpstest-bad latitudes.txt");
	}
	
	/**
	 * Test the case when the longitude of the point is outside the range for a valid longitude
	 * 
	 * @throws Exception if there was an issue initializing the Parser
	 */
	@Test
	public void test_BadLongitude() throws Exception {
		//Arrange
		expectException(SAXException.class, "invalid longitude value");
		GPSParserEventHandler handler = createHandler();
		Parser parser = new Parser(handler);

		//Act
		parser.parse(filePath + "gpstest-bad longitudes.txt");
	}
	
	/**
	 * Tests the case when the elevation is missing from a track point
	 * 
	 * @throws Exception if there was an issue initializing the Parser
	 */
	@Test
	public void test_MissingElevation() throws Exception {
		//Arrange
		expectException(SAXException.class, "elevation missing in trkpt element");
		GPSParserEventHandler handler = createHandler();
		Parser parser = new Parser(handler);

		//Act
		parser.parse(filePath + "gpstest-missing elevation.txt");
	}
	
	/**
	 * Tests the case when the time is missing from a track point
	 * 
	 * @throws Exception if there was an issue initializing the Parser
	 */
	@Test
	public void test_MissingTime() throws Exception {
		//Arrange
		expectException(SAXException.class, "time missing in trkpt element");
		GPSParserEventHandler handler = createHandler();
		Parser parser = new Parser(handler);

		//Act
		parser.parse(filePath + "gpstest-missing time.txt");
	}
	
	/**
	 * Tests the case when a time for a track point is not in a valid format
	 * 
	 * @throws Exception if there was an issue initializing the Parser
	 */
	@Test
	public void test_BadTime() throws Exception {
		//Arrange
		expectException(SAXException.class, "invalid time for trkpt");
		GPSParserEventHandler handler = createHandler();
		Parser parser = new Parser(handler);

		//Act
		parser.parse(filePath + "gpstest-bad times.txt");
	}
	
	/**
	 * Tests the case when a elevation element contains an element inside it
	 * 
	 * @throws Exception if there was an issue initializing the Parser
	 */
	@Test
	public void test_ElevationContainsInvalidElement() throws Exception {
		//Arrange
		expectException(SAXException.class, "ele elements cannot contain child elements");
		GPSParserEventHandler handler = createHandler();
		Parser parser = new Parser(handler);

		//Act
		parser.parse(filePath + "InvalidGPX4.txt");
	}
	
	/**
	 * Tests the case when a time element contains an element inside it
	 * 
	 * @throws Exception if there was an issue initializing the Parser
	 */
	@Test
	public void test_TimeContainsInvalidElement() throws Exception {
		//Arrange
		expectException(SAXException.class, "time elements cannot contain child elements");
		GPSParserEventHandler handler = createHandler();
		Parser parser = new Parser(handler);

		//Act
		parser.parse(filePath + "InvalidGPX5.txt");
	}
	
	/**
	 * Tests the case when a track points element contains an invalid element
	 * 
	 * @throws Exception if there was an issue initializing the Parser
	 */
	@Test
	public void test_PointContainsInvalidElement() throws Exception {
		//Arrange
		expectException(SAXException.class, "invalid element found in trkpt");
		GPSParserEventHandler handler = createHandler();
		Parser parser = new Parser(handler);

		//Act
		parser.parse(filePath + "InvalidGPX6.txt");
	}
	
	/**
	 * Basic test case for a valid GPX file
	 * 
	 * @throws Exception if there was an issue initializing the Parser
	 */
	@Test
	public void test_validGPXFile() throws Exception {
		//Arrange
		GPSParserEventHandler handler = createHandler();
		Parser parser = new Parser(handler);

		//Act
		parser.parse(filePath + "GPSTest1.gpx");

		//Assert
		assertEquals("GPS Test: 1 point.", handler.getGPSTrack().getName());

		List<GPSPoint> points = handler.getGPSTrack().getPoints();
		assertEquals(1, points.size());

		assertPoint(43.3, -87.9, 500, "2016-02-10T13:00", points.get(0));
	}
	
	/**
	 * Test case for a GPX file with many points
	 * 
	 * @throws Exception if there was an issue initializing the Parser
	 */
	@Test
	public void test_validMultiplePoints() throws Exception {
		//Arrange
		GPSParserEventHandler handler = createHandler();
		Parser parser = new Parser(handler);

		//Act
		parser.parse(filePath + "GPSTest10.gpx");

		//Assert
		assertEquals("GPS Test: 10 points. 73.9km", handler.getGPSTrack().getName());

		List<GPSPoint> points = handler.getGPSTrack().getPoints();
		assertEquals(10, points.size());

		assertPoint(43.3, -88, 1000, "2016-02-10T13:10", points.get(0));
		assertPoint(43.3, -88.1, 1500, "2016-02-10T13:20", points.get(1));
		assertPoint(43.3, -88.2, 2000, "2016-02-10T13:30", points.get(2));
		assertPoint(43.3, -88.3, 2500, "2016-02-10T13:40", points.get(3));
		assertPoint(43.3, -88.4, 3000, "2016-02-10T13:50", points.get(4));
		assertPoint(43.3, -88.5, 2500, "2016-02-10T14:00", points.get(5));
		assertPoint(43.3, -88.6, 2000, "2016-02-10T14:10", points.get(6));
		assertPoint(43.3, -88.7, 1500, "2016-02-10T14:20", points.get(7));
		assertPoint(43.3, -88.8, 1000, "2016-02-10T14:30", points.get(8));
		assertPoint(43.3, -88.9, 500, "2016-02-10T14:40", points.get(9));
	}

	private void assertPoint(double lat, double lon, double elevation, String time, GPSPoint actual) {
		assertEquals(lat, actual.getLatitude(), DELTA);
		assertEquals(lon, actual.getLongitude(), DELTA);
		assertEquals(elevation, actual.getElevation(), DELTA);
		assertEquals(time, actual.getTime().toString());
	}

	private GPSParserEventHandler createHandler() {
		GPSParserEventHandler handler = new GPSParserEventHandler();
		handler.enableLogging(false);
		return handler;
	}

    private void expectException(Class<? extends Throwable> exceptionClass, String message) {
    	thrown.expect(exceptionClass);
    	thrown.expectMessage(message);
    }
}
